#!/usr/bin/env python3

from curses.ascii import NUL
import rclpy
from rclpy.node import Node

from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import JointState
from sensor_msgs.msg import Range
from sensor_msgs.msg import Image

import cv2
from cv_bridge import CvBridge


class EPuckController(Node):
    def __init__(self):
        super().__init__('epuck_controller')

        self.cmd_publisher_ = self.create_publisher(
            Float64MultiArray, '/wheels_velocity_controller/commands', 10)

        self.joint_state_subscription_ = self.create_subscription(
            JointState,
            '/joint_states',
            self.joint_state_callback,
            10)

        self.laser_distances = []
        self.laser_subscriptions_ = []
        for i in reversed(range(8)):
            self.laser_distances.append(1000)
            self.laser_subscriptions_.append(
                self.create_subscription(
                    Range,
                    '/ps%d/out' % i,
                    lambda msg, i=i: self.laser_callback(msg, i),
                    10)
            )

        self.camera_subscription_ = self.create_subscription(
            Image,
            '/camera/image_raw',
            self.camera_callback,
            10)

        self.cmd_ = Float64MultiArray(data=[0, 0])

        self.cv_bridge_ = CvBridge()

        self.start_time_ = self.get_clock().now().seconds_nanoseconds()[0]
        self.run_timer_ = self.create_timer(0.1, self.run)

    def run(self):
        if self.laser_distances[0] > 0.1:
            self.cmd_.data = [1.0, -1.0]
        else:
            self.cmd_.data = [0.0, 0.0]

        self.cmd_publisher_.publish(self.cmd_)

    def joint_state_callback(self, msg):
        self.get_logger().info(
            "\nWheels\n\tname: %s\n\tposition: %s\n\tvelocity: %s" %
            (msg.name, msg.position, msg.velocity))

    def laser_callback(self, msg, index):
        self.laser_distances[index] = msg.range

    def camera_callback(self, msg):
        self.cv_image = self.cv_bridge_.imgmsg_to_cv2(msg)
        cv2.imshow("robot camera", self.cv_image)
        cv2.waitKey(1)


def main(args=None):
    rclpy.init(args=args)

    epuck_controller = EPuckController()

    try:
        rclpy.spin(epuck_controller)
    finally:
        rclpy.shutdown()


if __name__ == '__main__':
    main()
