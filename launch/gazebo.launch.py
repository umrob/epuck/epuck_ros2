from launch import LaunchDescription
import launch.actions
from ament_index_python.packages import get_package_share_directory
from launch.actions import IncludeLaunchDescription, ExecuteProcess
from launch.launch_description_sources import PythonLaunchDescriptionSource
import launch_ros
import os


def generate_launch_description():
    pkg_share = os.path.join(
        get_package_share_directory('epuck_ros2'))

    pkg_gazebo_ros = get_package_share_directory('gazebo_ros')

    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_gazebo_ros, 'launch', 'gazebo.launch.py')),
        launch_arguments={'verbose': 'true', 'gui': 'true'}.items()

    )

    static_transform_publisher = launch_ros.actions.Node(
        package="tf2_ros",
        executable="static_transform_publisher",
        name="tf_footprint_base",
        arguments=["0", "0", "0", "0", "0", "0", "base_link", "base_footprint"])

    spawn_gazebo_model = launch_ros.actions.Node(
        package="gazebo_ros",
        executable="spawn_entity.py",
        name="spawn_gazebo_model",
        arguments=["-topic", "/robot_description", "-entity", "epuck"])

    ros_distro = os.getenv('ROS_DISTRO')
    controller_state = 'start' if ros_distro == 'foxy' else 'active'

    load_joint_state_controller = ExecuteProcess(
        cmd=['ros2', 'control', 'load_controller', '--set-state', controller_state,
             'joint_state_broadcaster'],
        output='screen'
    )

    load_wheels_velocity_controller = ExecuteProcess(
        cmd=['ros2', 'control', 'load_controller', '--set-state', controller_state,
             'wheels_velocity_controller'],
        output='screen'
    )

    return LaunchDescription([
        gazebo,
        static_transform_publisher,
        spawn_gazebo_model,
        load_joint_state_controller,
        load_wheels_velocity_controller
    ])
